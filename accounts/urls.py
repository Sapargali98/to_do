from django.urls import path
from accounts.views import login_view,logout_view,RegisterView,create_profile
from .views import profile, change_password
app_name = 'accounts'
urlpatterns = [
    path('profile/', profile, name='profile'),
    path('create/', create_profile, name='create_profile'),
    path('change_password/', change_password, name='change_password'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
]
