from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render,redirect
from django.urls import reverse, reverse_lazy
from todo.forms import TaskFilterForm, TaskForm,SearchForm
from todo.models import Task 
from django.views.generic import ListView, DetailView, DeleteView,UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
def home(request):
    return render(request, 'home.html')
@login_required
def task_create(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todo:task-list')  
    else:
        form = TaskForm()
    return render(request, 'todo/task_form.html', {'form': form})
class TaskListView(ListView):
    model= Task
    template_name = 'task_list.html'
    form = SearchForm
    paginate_by = 5
    paginate_orphans = 1
    search_value = None 
    def get_queryset(self):
        queryset = super().get_queryset()
        form = TaskFilterForm(self.request.GET)
        if form.is_valid():
            queryset = form.filter_queryset(queryset)
        return queryset
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = TaskFilterForm(self.request.GET)
        return context
# class TaskUpdateView(LoginRequiredMixin, UpdateView):
#     model = Task
#     form_class = TaskForm
#     template_name = 'task_form.html'

#     def get_object(self, queryset=None):
#         obj = super().get_object(queryset)
#         if self.request.user not in obj.users.all():
#             raise PermissionDenied
#         return obj

#     def get_success_url(self):
#         return reverse('todo:task-detail', kwargs={'pk': self.object.pk})
def task_update(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save()
            messages.success(request, 'Task updated successfully!')
            return redirect('todo:task-list')
    else:
        form = TaskForm(instance=task)

    return render(request, 'todo/task_form.html', {'form': form})

# class TaskDeleteView(LoginRequiredMixin,DeleteView):
#     model = Task
#     template_name = 'task_delete.html'
#     def get_queryset(self):
#         return super().get_queryset().filter(users=self.request.user)

#     def get_success_url(self):
#         return reverse_lazy('task-list')
# class TaskDetailView(LoginRequiredMixin,DetailView):
#     model = Task
#     template_name = 'task_detail.html'
#     context_object_name = 'task'
#     def get_queryset(self):
#         return super().get_queryset().filter(users=self.request.user)
@login_required
def task_delete(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if request.method == 'POST':
        task.delete()
        messages.success(request, 'Task deleted successfully!')
        return redirect('todo:task_list')
    return render(request, 'todo/task_delete.html', {'task': task})
def task_detail(request, pk):
    task = get_object_or_404(Task, pk=pk)
    return render(request, 'todo/task_detail.html', {'task': task})