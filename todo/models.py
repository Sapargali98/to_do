from django.db import models
from django.contrib.auth.models import User

from projects.models import Project

class Task(models.Model):
    STATUS_CHOICES = (
        ('in progress', 'In Progress'),
        ('completed', 'Completed'),
        ('pending', 'Pending'),
    )
    title = models.CharField(max_length=200)
    description = models.TextField()
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    users = models.ManyToManyField(User, related_name='tasks', blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='tasks',default=None)

    def __str__(self):
        return self.title
