from django.urls import path
from . import views
from .views import   TaskListView
app_name = 'todo'
urlpatterns = [
    path('', TaskListView.as_view(), name='task-list'),
    path('create/', views.task_create, name='task-create'),
    path('update/<int:pk>/',views.task_update, name='task-update'),
    path('delete/<int:pk>/', views.task_delete, name='task-delete'),
    path('detail/<int:pk>',views.task_detail, name='task-detail'),
]
